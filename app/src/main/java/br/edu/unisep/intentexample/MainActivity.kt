package br.edu.unisep.intentexample

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import br.edu.unisep.intentexample.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnGoSecond.setOnClickListener { goToSecond() }
        binding.btnGoThird.setOnClickListener { goToThird() }
    }

    private fun goToSecond() {
        val intentToSecond = Intent(this, SecondActivity::class.java).apply {
            putExtra("extra-message", "Message from main!")
        }
        startActivity(intentToSecond)
    }

    private val activityThirdLauncher = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()) { result ->

        if (result.resultCode == RESULT_OK) {
            val message = result.data?.getStringExtra("result-message")
            Toast.makeText(this, message, Toast.LENGTH_LONG).show()
        }
    }

    private fun goToThird() {
        val intentToThird = Intent(this, ThirdActivity::class.java)
        activityThirdLauncher.launch(intentToThird)
    }

}