package br.edu.unisep.intentexample

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import br.edu.unisep.intentexample.databinding.ActivityThirdBinding

class ThirdActivity : AppCompatActivity() {

    private lateinit var binding: ActivityThirdBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityThirdBinding.inflate(layoutInflater)

        setContentView(binding.root)
        binding.btnGoBack.setOnClickListener { goBackToFirst() }
    }

    private fun goBackToFirst() {
        val intentResult = Intent().apply {
            putExtra("result-message", "Success from third!")
        }
        setResult(RESULT_OK, intentResult)
        finish()
    }
}