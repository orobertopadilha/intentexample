package br.edu.unisep.intentexample

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import br.edu.unisep.intentexample.databinding.ActivitySecondBinding

class SecondActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySecondBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySecondBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initialize()
    }

    private fun initialize() {
        binding.btnGoBack.setOnClickListener { goBackToMain() }

        val message = intent.getStringExtra("extra-message")
        binding.tvMessage.text = message
    }

    private fun goBackToMain() {
        finish()
    }
}